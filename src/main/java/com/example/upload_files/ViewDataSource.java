package com.example.upload_files;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "viewEntityManagerFactory",
        transactionManagerRef = "viewTransactionManager", basePackages = {"com.comohogar.sukasa.view"})
public class ViewDataSource {

    @Bean(name = "vDataSource")
    @ConfigurationProperties(prefix = "vdatasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "viewEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean barEntityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("vDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource).packages("com.comohogar.sukasa.view").persistenceUnit("view")
                .build();
    }

    @Bean(name = "viewTransactionManager")
    public PlatformTransactionManager barTransactionManager(
            @Qualifier("viewEntityManagerFactory") EntityManagerFactory barEntityManagerFactory) {
        return new JpaTransactionManager(barEntityManagerFactory);
    }

}
