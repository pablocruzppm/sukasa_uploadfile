package com.example.upload_files.products.api.controller;


import com.example.upload_files.products.application.command.CreateResourceTempCommand;
import io.jkratz.mediator.core.Mediator;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/resource")
public class ResourceController {

    private final Mediator mediator;

    public ResourceController(Mediator mediator) {
        this.mediator = mediator;
    }

    @GetMapping("/resourceTemp")
    public ResponseEntity<?> createResourceTemp() {
        this.mediator.dispatch(new CreateResourceTempCommand());
        return ResponseEntity.ok().build();
    }
}
