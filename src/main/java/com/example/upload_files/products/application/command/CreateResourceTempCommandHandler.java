package com.example.upload_files.products.application.command;

import com.example.upload_files.products.domain.entities.ResourceTemp;
import com.example.upload_files.products.domain.interfaces.IResourceTempRepository;
import io.jkratz.mediator.core.CommandHandler;
import io.jkratz.mediator.core.Mediator;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

@Component
public class CreateResourceTempCommandHandler implements CommandHandler<CreateResourceTempCommand> {

    //@Value("${image.size}")
    private Integer imageSize = 8192;


    private final Mediator mediator;

    public CreateResourceTempCommandHandler(Mediator mediator, IResourceTempRepository resourceTempRepository) {
        this.mediator = mediator;
        this.resourceTempRepository = resourceTempRepository;
    }

    private final IResourceTempRepository resourceTempRepository;


    @Override
    public void handle(@NotNull CreateResourceTempCommand createResourceTempCommand) {
        int a = 1;
        List<ResourceTemp> listResourceTemp = resourceTempRepository.findByActive(0);
    }
}
