package com.example.upload_files.products.application.dtos.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateFileResponse {
    private String id;
    private String url;

    public CreateFileResponse(String id, String url) {
        this.id = id;
        this.url = url;
    }
}