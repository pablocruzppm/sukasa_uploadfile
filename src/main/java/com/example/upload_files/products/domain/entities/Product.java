package com.example.upload_files.products.domain.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Accessors(chain = true)
@NoArgsConstructor
public class Product {

    private String id;

    private String name;

    private String location;

    private String slug;

    private String shortDescription;

    private String longDescription;

    private String features;

    private Integer guarantee;

    private Boolean ego;

    private Double price;

    private Double priceIva;

    private Double sukasaPrice;

    private Double sukasaPriceIva;

    private Double sukasaPriceDiscount;

    private String packageDimensions;

    private String packageWeight;

    private String packageDeep;

    private String packageWidth;

    private String packageHeight;

    private Boolean clickGo;

    private Date entryDate;

    private String configurableProductId;

    private String seoParamsId;

    private String brandId;

    private Boolean hasIva;

    private String installationKitId;

    private Boolean pickUp;

    private String collectionProductId;

    private String brandReference;

    private Date createdAt;

    private Date updatedAt;

    private Date deleteAt;

    private Boolean active;

    private Integer photoStatus;

    private Integer cmItemId;

    private String cmDescription;

    private String cmTrademark;

    private String cmModel;

    private String cmInternalCode;

    private Integer cmStock;

    private Boolean cmReadPpm;

    private Boolean cmSpecialDispatch;

    private String cmWebModel;

    private String cmStatus;

    private String cmEan13;

    private Boolean cmAllHome;

    private Boolean cmChristmasRoom;

    private Boolean cmChargeIva;

    private Double cmItmPvpAfNiva;

    private Double cmItmPvpAfIva;

    private Double cmItmPvpNafNiva;

    private Double cmItmPvpNafIva;

    private String stype;

    private Date productCompletedDate;

    private Date photoUploadDate;

    // COMOHOGAR

    private Boolean cmSukasa;

    private Integer cmTrademarkId;

    private List<Resource> resources;

}

