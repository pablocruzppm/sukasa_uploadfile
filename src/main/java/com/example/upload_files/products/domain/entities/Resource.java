package com.example.upload_files.products.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

@Setter
@Getter
@ToString
@NoArgsConstructor
@Accessors(chain = true)

public class Resource {

    private String id;

    private String resourceUrl;

    private Boolean active;

    private Integer nOrder;

    private String type;

    private Boolean coverImage;

    private String description;

    private String fileBase64;

    private String origin;

    @JsonIgnore
    private Date createdAt;

    @JsonIgnore
    private Date updatedAt;

}
