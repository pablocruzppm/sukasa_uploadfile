package com.example.upload_files.products.domain.entities;

import lombok.Getter;

@Getter
public class ResourceTemp {

    private String id;

    private String resourceUrl;

    private String internalCode;

    private Integer active;

}