package com.example.upload_files.products.domain.interfaces;

import com.example.upload_files.products.domain.entities.Product;

import java.util.List;

public interface IProductRepository {

    Product save(Product product);


    void update(Product product);

    List<Product> findByCmInternalCode(String cmInternalCode);
}
