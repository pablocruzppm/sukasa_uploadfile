package com.example.upload_files.products.domain.interfaces;

import com.example.upload_files.products.domain.entities.Resource;

public interface IResourceRepository {

        Resource save(Resource resource);

        void update(Resource resource);

}
