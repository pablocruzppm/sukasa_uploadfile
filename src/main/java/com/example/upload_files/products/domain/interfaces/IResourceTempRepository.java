package com.example.upload_files.products.domain.interfaces;

import com.example.upload_files.products.domain.entities.ResourceTemp;

import java.util.List;

public interface IResourceTempRepository {

    List<ResourceTemp> findByActive(Integer active);
}
