package com.example.upload_files.products.infrastructure.apiexternal;


import com.example.upload_files.products.application.dtos.response.CreateFileResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

@FeignClient(name = "files" , url = "${feign-uri-file}")
public interface FilewsClient {

    @PostMapping("files/base64")
    CreateFileResponse saveFileS3(@RequestHeader Map<String, String> headerMap, @RequestBody String file);
}