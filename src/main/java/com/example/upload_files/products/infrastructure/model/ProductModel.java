package com.example.upload_files.products.infrastructure.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "products")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
public class ProductModel {


    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name="slug")
    private String slug;

    @Column(name = "short_description", columnDefinition = "text")
    private String shortDescription;

    @Column(name = "long_description", columnDefinition = "text")
    private String longDescription;

    @Column(name="features", columnDefinition = "text")
    private String features;

    @Column(name = "guarantee")
    private Integer guarantee;

    @Column(name = "ego")
    private Boolean ego;



    @Column(name = "package_dimensions")
    private String packageDimensions;

    @Column(name = "package_weight")
    private String packageWeight;

    @Column(name = "package_deep")
    private String packageDeep;

    @Column(name = "package_width")
    private String packageWidth;

    @Column(name = "package_height")
    private String packageHeight;

    @Column(name = "click_go")
    private Boolean clickGo;

    @Column(name = "entry_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;

    @Column(name = "configurable_product_id")
    private String configurableProductId;

    @Column(name = "brand_id", nullable = true)
    private String brandId;



    @Column(name = "installation_kit_id")
    private String installationKitId;

    @Column(name = "pickup")
    private Boolean pickUp;

    @Column(name = "collection_product_id")
    private String collectionProductId;

    @Column(name = "stype")
    private String type;

    @CreatedDate
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Column(name = "delete_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteAt;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "photo_status")
    private Integer photoStatus;

    @Column(name = "product_completed_date")
    private Date productCompletedDate;

    @Column(name = "photo_upload_date")
    private Date photoUploadDate;

    // COMOHOGAR

    @Column(name = "cm_item_id")
    private Integer cmItemId;

    @Column(name = "cm_description")
    private String cmDescription;

    @Column(name = "cm_trademark")
    private String cmTrademark;

    @Column(name = "cm_model")
    private String cmModel;

    @Column(name = "cm_internal_code")
    private String cmInternalCode;

    @Column(name = "cm_stock")
    private Integer cmStock;

    @Column(name = "cm_read_ppm")
    private Boolean cmReadPpm;

    @Column(name = "cm_special_dispatch")
    private Boolean cmSpecialDispatch;

    @Column(name = "cm_web_model")
    private String cmWebModel;

    @Column(name = "cm_status")
    private String cmStatus;

    @Column(name = "cm_ean13")
    private String cmEan13;

    @Column(name = "cm_all_home")
    private Boolean cmAllHome;

    @Column(name = "cm_christmas_room")
    private Boolean cmChristmasRoom;

    @Column(name = "cm_charge_iva")
    private Boolean cmChargeIva;

    @Column(name = "cm_itm_pvp_af_niva")
    private Double cmItmPvpAfNiva;

    @Column(name = "cm_itm_pvp_af_iva")
    private Double cmItmPvpAfIva;

    @Column(name = "cm_itm_pvp_naf_niva")
    private Double cmItmPvpNafNiva;

    @Column(name = "cm_itm_pvp_naf_iva")
    private Double cmItmPvpNafIva;

    @Column(name = "cm_trademark_id")
    private Integer cmTrademarkId;

    @Column(name = "cm_delivery_type")
    private Integer cmDeliveryType;

    @Column(name = "cm_locked_sk")
    private Boolean cmLockedSK;

    @Column(name = "cm_locked_th")
    private Boolean cmLockedTH;

    @Column(name = "cm_locked_sn")
    private Boolean cmLockedSN;

    @Column(name = "filter_configurable")
    private String filterConfigurable;

    @Column(name = "migrations_es")
    private Integer migrationES;

    @JoinTable(name = "products_resource",
            joinColumns = @JoinColumn(name = "id_product", nullable = false),
            inverseJoinColumns = @JoinColumn(name="id_resource", nullable = false))
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ResourceModel> resources;

    private Boolean enabled;

    @Column(name = "user_id")
    private String userId;

    @PrePersist
    public void prePersist() {
        id = UUID.randomUUID().toString();
        createdAt = new Date();
        enabled = true;

    }
}
