package com.example.upload_files.products.infrastructure.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "resources")
@Setter
@Getter
@ToString
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
public class ResourceModel {

    @Id
    private String id;

    @Column(name = "resource_url" )
    private String resourceUrl;

    @Column(name = "active" )
    private Boolean active;

    @Column(name = "n_order" )
    private Integer nOrder;

    @Column(name = "type_s" )
    private String type;

    @Column(name = "cover_image" )
    private Boolean coverImage;

    @Column(name = "description" )
    private String description;

    @Lob
    @Column(name = "file_base64" )
    private String fileBase64;

    @Column(name = "origin" )
    private String origin;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @CreatedDate
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @PrePersist
    public void prePersist() {
        id = UUID.randomUUID().toString();
        createdAt = new Date();
        active = true;
    }
}
