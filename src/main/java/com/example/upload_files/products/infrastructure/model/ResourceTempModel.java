package com.example.upload_files.products.infrastructure.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "resources_temp")
@Setter
@Getter
@ToString
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
public class ResourceTempModel {

    @Id
    private String id;

    @Column(name = "resource_url" )
    private String resourceUrl;

    @Column(name = "internal_code" )
    private String internalCode;

    @Column(name = "n_order" )
    private Integer nOrder;

    @Column(name = "cover_image" )
    private boolean coverImage;

    @Column(name = "active" )
    private Integer active;


    @PrePersist
    public void prePersist() {
        id = UUID.randomUUID().toString();
        active = 1;
    }
}
