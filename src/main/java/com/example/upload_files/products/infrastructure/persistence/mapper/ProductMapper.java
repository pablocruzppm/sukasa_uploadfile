package com.example.upload_files.products.infrastructure.persistence.mapper;

import com.example.upload_files.products.domain.entities.Product;
import com.example.upload_files.products.infrastructure.model.ProductModel;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ResourceMapper.class})
public interface ProductMapper {

    Product toProduct(ProductModel productModel);

    List<Product> toProducts(List<ProductModel> products);

    @InheritInverseConfiguration
    ProductModel toProductModel(Product product);

    @InheritInverseConfiguration
    List<ProductModel> toProductModels(List<Product> prpducts);

}