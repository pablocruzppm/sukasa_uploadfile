package com.example.upload_files.products.infrastructure.persistence.mapper;

import com.example.upload_files.products.domain.entities.Resource;
import com.example.upload_files.products.infrastructure.model.ResourceModel;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ResourceMapper {

    Resource toResource(ResourceModel model);

    @InheritInverseConfiguration
    ResourceModel toResourceModel(Resource resource);

    List<Resource> toResources(List<ResourceModel> resourceModels);

    @InheritInverseConfiguration
    List<ResourceModel> toResourceModels(List<Resource> resource);
}