package com.example.upload_files.products.infrastructure.persistence.mapper;

import com.example.upload_files.products.domain.entities.ResourceTemp;
import com.example.upload_files.products.infrastructure.model.ResourceTempModel;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ResourceTempMapper {

    ResourceTemp toResourceTemp(ResourceTempModel model);

    @InheritInverseConfiguration
    ResourceTempModel toResourceTempModel(ResourceTemp ResourceTemp);

    List<ResourceTemp> toResourceTemps(List<ResourceTempModel> ResourceTempModels);

    @InheritInverseConfiguration
    List<ResourceTempModel> toResourceTempModels(List<ResourceTemp> ResourceTemp);
}
