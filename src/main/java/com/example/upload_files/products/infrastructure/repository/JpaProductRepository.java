package com.example.upload_files.products.infrastructure.repository;

import com.example.upload_files.products.infrastructure.model.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JpaProductRepository extends JpaRepository<ProductModel, String> {

    List<ProductModel> findByCmReadPpm(Boolean readPpm);

    ProductModel findByCmItemId(Integer cmItemId);

    List<ProductModel> findByCmInternalCode(String cmInternalCode);
}
