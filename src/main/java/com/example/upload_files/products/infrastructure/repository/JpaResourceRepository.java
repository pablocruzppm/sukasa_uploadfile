package com.example.upload_files.products.infrastructure.repository;

import com.example.upload_files.products.infrastructure.model.ResourceModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaResourceRepository extends JpaRepository<ResourceModel, String> {
}
