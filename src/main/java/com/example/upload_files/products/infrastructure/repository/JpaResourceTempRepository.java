package com.example.upload_files.products.infrastructure.repository;
import com.example.upload_files.products.infrastructure.model.ResourceTempModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JpaResourceTempRepository extends JpaRepository<ResourceTempModel, String> {
    List<ResourceTempModel> findByActive(Integer active);
}
