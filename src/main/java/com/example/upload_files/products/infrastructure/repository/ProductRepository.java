package com.example.upload_files.products.infrastructure.repository;

import com.example.upload_files.products.domain.entities.Product;
import com.example.upload_files.products.domain.interfaces.IProductRepository;
import com.example.upload_files.products.infrastructure.model.ProductModel;
import com.example.upload_files.products.infrastructure.persistence.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.*;

@Repository
public class ProductRepository implements IProductRepository {


    @Autowired
    private JpaProductRepository jpaProductRepository;

    @Autowired
    private ProductMapper productMapper;


    @Override
    public Product save(Product product) {
        ProductModel productModel = productMapper.toProductModel(product);
        productModel=this.jpaProductRepository.save(productModel);
        return productMapper.toProduct(productModel);
    }

    @Override
    public void update(Product product) {
        product.setUpdatedAt(new Date());
        ProductModel productModel = productMapper.toProductModel(product);
        jpaProductRepository.save(productModel);
    }

    @Override
    public List<Product> findByCmInternalCode(String cmInternalCode) {
        List<ProductModel> products = jpaProductRepository.findByCmInternalCode(cmInternalCode);
        return productMapper.toProducts(products);
    }

}
