package com.example.upload_files.products.infrastructure.repository;

import com.example.upload_files.products.domain.entities.Resource;
import com.example.upload_files.products.domain.interfaces.IResourceRepository;
import com.example.upload_files.products.application.dtos.response.CreateFileResponse;
import com.example.upload_files.products.infrastructure.apiexternal.FilewsClient;
import com.example.upload_files.products.infrastructure.persistence.mapper.ResourceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Repository
public class ResourceAdminRepository implements IResourceRepository {

    @Autowired
    private JpaResourceRepository repository;

    @Autowired
    private ResourceMapper mapper;

    @Autowired
    private FilewsClient filewsClient;

    @Value("${X-Bucket-Name}")
    private String BucketName;

    @PersistenceContext
    EntityManager entityManager;


    @Override
    @Transactional
    public Resource save(Resource resource)  {

        resource.setId(UUID.randomUUID().toString());
        var resourceModel = mapper.toResourceModel(resource);
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Bucket-Name", BucketName);
        headers.put("X-Bucket-Path", resource.getOrigin());
        headers.put("X-Access-Type", "public");
        headers.put("X-File-Name", resourceModel.getId());
        headers.put("X-OwnerID", "1");
        headers.put("X-ContentTypeToExtension", resource.getType());
        CreateFileResponse fileResp;
        try {
                fileResp = filewsClient.saveFileS3(headers, resource.getFileBase64());
            //fileResp = new CreateFileResponse("3555cf40-61dc-4949-93e9-bcf0bc77c88d","https://ch-develop-resources.s3.us-east-1.amazonaws.com/public/images/test0001.png");
            resourceModel.setFileBase64("1");
            resourceModel.setResourceUrl(fileResp.getUrl());
            var resourceToSave = repository.save(resourceModel);
            return mapper.toResource(resourceToSave);
        }catch (Exception e){
            int a =3;
        }

        return null;
    }

    @Override
    public void update(Resource resource) {
        var resourceModel = mapper.toResourceModel(resource);
        resourceModel.setUpdatedAt(new Date());
        repository.save(resourceModel);
    }




}
