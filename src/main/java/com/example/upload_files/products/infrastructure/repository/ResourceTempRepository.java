package com.example.upload_files.products.infrastructure.repository;

import com.example.upload_files.products.domain.entities.Product;
import com.example.upload_files.products.domain.entities.ResourceTemp;
import com.example.upload_files.products.domain.interfaces.IProductRepository;
import com.example.upload_files.products.domain.interfaces.IResourceRepository;
import com.example.upload_files.products.domain.interfaces.IResourceTempRepository;
import com.example.upload_files.products.infrastructure.model.ProductModel;
import com.example.upload_files.products.infrastructure.model.ResourceModel;
import com.example.upload_files.products.infrastructure.model.ResourceTempModel;
import com.example.upload_files.products.infrastructure.persistence.mapper.ProductMapper;
import com.example.upload_files.products.infrastructure.persistence.mapper.ResourceMapper;
import com.example.upload_files.products.infrastructure.persistence.mapper.ResourceTempMapper;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Base64;
import java.util.List;

@Repository
public class ResourceTempRepository implements IResourceTempRepository {

    @Value("resource-folder")
    public static String USERS_PABLO_CRUZ_DESKTOP_RESOURCES_SUKASA;
    @Autowired
    private ResourceTempMapper mapper;

    @Autowired
    private JpaResourceTempRepository repository;

    @Autowired
    private IProductRepository productRepository;

    @Autowired
    private IResourceRepository resourceRepository;

    @Autowired
    private ResourceMapper resourcesMapper;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<ResourceTemp> findByActive(Integer active) {
        List<ResourceTempModel> listResourceTemp = repository.findByActive(active);
        for (ResourceTempModel resourcesTempModel : listResourceTemp) {
            try {
                if (!resourcesTempModel.getResourceUrl().isEmpty()) {
                    BufferedInputStream in = new BufferedInputStream(new URL(resourcesTempModel.getResourceUrl()).openStream());
                    FileOutputStream fileOutputStream = new FileOutputStream(USERS_PABLO_CRUZ_DESKTOP_RESOURCES_SUKASA + resourcesTempModel.getId() + ".png");
                    byte dataBuffer[] = new byte[8192];
                    int bytesRead;
                    while ((bytesRead = in.read(dataBuffer, 0, 8192)) != -1) {
                        fileOutputStream.write(dataBuffer, 0, bytesRead);
                    }

                    String resourcePath =  USERS_PABLO_CRUZ_DESKTOP_RESOURCES_SUKASA+resourcesTempModel.getId()+".png";
                    byte[] fileContent = FileUtils.readFileToByteArray(new File(resourcePath));
                    String encodedString = Base64.getEncoder().encodeToString(fileContent);
                    String cmInternalCode = resourcesTempModel.getInternalCode().replaceAll("\'","");
                    List<Product> products = productRepository.findByCmInternalCode(cmInternalCode);
                    for(Product productFromDatabase: products){
                        ResourceModel resourceModel = new ResourceModel();
                        resourceModel.setFileBase64(encodedString);
                        resourceModel.setType("image/jpg");
                        resourceModel.setOrigin("original-resources/product/"+productFromDatabase.getId()+"/image");
                        resourceModel.setCoverImage(resourcesTempModel.isCoverImage());
                        resourceModel.setNOrder(resourcesTempModel.getNOrder());
                        resourceModel = resourcesMapper.toResourceModel(resourceRepository.save(resourcesMapper.toResource(resourceModel)));
                        resourceRepository.update(resourcesMapper.toResource(resourceModel));
                        productFromDatabase.getResources().add(resourcesMapper.toResource(resourceModel));
                        ProductModel productModel = productMapper.toProductModel(productFromDatabase);
                        productRepository.update(productMapper.toProduct(productModel));
                        resourcesTempModel.setActive(2);
                        repository.save(resourcesTempModel);
                    }
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mapper.toResourceTemps(repository.findByActive(active));
    }

}
